from flask import Flask, jsonify, request
from flask.ext.pymongo import PyMongo
from flask_cors import CORS, cross_origin

from oauth2client import client, crypt

# Initial Configuration and Initialization
postDataApp = Flask(__name__)
postDataApp.config['MONGO_DBNAME'] = 'aspire'
postDataApp.config['MONGO_URI'] = 'mongodb://loki:lokibg2@ds119598.mlab.com:19598/aspire'
mongo = PyMongo(postDataApp)
CORS(postDataApp)


# API to fetch all data posted
@postDataApp.route('/fetch', methods=["GET"])
def fetch_all():
    data_reference = mongo.db.data
    results = []

    for data in data_reference.find():
        results.append({'userID': data['uid'], 'text': data['content'], 'timestamp': data['timestamp']})
    return jsonify({'result': results});


# API to fetch data of single user
@postDataApp.route('/fetch/<uid>', methods=["GET"])
def fetch_one(uid):
    data_reference = mongo.db.data
    results = []
    for data in data_reference.find({"uid": str(uid)}):
        results.append({'userID': data['uid'], 'text': data['content'], 'timestamp': data['timestamp']})
    return jsonify({'result': results})


# API to post user data
@postDataApp.route('/post', methods=["POST"])
def post_data():
    data_reference = mongo.db.data
    user_id = request.form['uid']
    text = request.form['content']
    timestamp = request.form['timestamp']

    data_id = data_reference.insert({'uid': user_id, 'content': text, 'timestamp': timestamp})
    new_data = data_reference.find_one({'_id': data_id})

    result = {'userID': new_data['uid'], 'text': new_data['content'], 'timestamp': new_data['timestamp']}

    return jsonify({'result': result})


@postDataApp.route('/login', methods=["POST", "OPTIONS"])
def login():
    token = request.form['id_token']
    client_id = '867566842721-nm9rujut9s29shvrkjiqcsmcm03ggae6.apps.googleusercontent.com'
    try:
        idinfo = client.verify_id_token(token, client_id)
        if idinfo['aud'] not in [client_id]:
            raise crypt.AppIdentityError("Unrecognized client.")
        if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
            raise crypt.AppIdentityError("Wrong issuer.")

    except crypt.AppIdentityError:
        # Invalid token
        return "Error!"

    return jsonify({'uid': idinfo['sub'], 'name': idinfo['name']})

if __name__ == '__main__':
    postDataApp.run(debug=True)