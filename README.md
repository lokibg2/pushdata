# PushData 

Home Project for Aspi.re Challenge

### Tech used used

* HTML5, MaterializeCSS, AngularJS (Frontend)
* Python Flask, MongoDB-mlab (Backend & Storage) 
* PythonAnywhere & Firebase (Deployment) 


### Description
* The app has been built as per the requirements specified.
* The frontend and backend have been deployed separately and linked together.
* The backend is built upon web service APIs (as the job requirement mentioned the need for SOA expertise).
* The frontend is built as per Angular Standards with the help of services, controllers and directives(predefined and custom).
* MaterializeCSS has been used as the responsive frontend framework in order to build the app as per the material design standards.

### Designed by

* Lokeshwaran K
* http://linkedin.com/in/lokeshwarank