'use strict';

// App Level Module
angular.module('pushData', [
    'ui.router',
    'pushData.login',
    'pushData.myAccount',
    'ngCookies'
]).config(['$urlRouterProvider', function ($urlRouterProvider) {
    // Landing Page route specification
    $urlRouterProvider.otherwise('/login');

}])
    .controller('MainCtrl', function ($scope, $rootScope, userService) {
        // Index Page Controller
        $rootScope.toggleButton = true;
        $scope.logOut = userService.logOut;
    });
