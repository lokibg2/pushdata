'use strict';

angular.module('pushData.login', ['ui.router', 'ngCookies'])
// Login Module
    .config(['$stateProvider', function ($stateProvider) {
        // Login State
        $stateProvider.state('login', {
            url: '/login',
            templateUrl: 'login/login.html',
            controller: 'LoginCtrl'
        })
    }])

    .controller('LoginCtrl', ['$scope', '$rootScope', 'userService', '$state', '$cookies', function ($scope, $rootScope, userService, $state, $cookies) {
        // Login Controller
        $rootScope.preLoad = false;
        $scope.loggedIn = true;
        if ($cookies.get('name') && $cookies.get('name') !== '') {
            // Check if User is logged in
            userService.user.name = $cookies.get('name');
            userService.user.uid = $cookies.get('uid');
            $scope.loggedIn = false;
            // Redirect to My Account Page
            $state.go('myAccount');
        }
    }])


    .service('userService', ['$http', '$rootScope', '$cookies', '$state', function ($http, $rootScope, $cookies, $state) {
        // User Service to manage login & logout
        this.user = {}; // Current User Object

        let config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        };
        let baseURL = 'https://lokibg2.pythonanywhere.com';

        // Login Method
        this.login = (googleUser) => {
            let data = $.param({
                id_token: googleUser.Zi.id_token
            });
            return $http.post(`${baseURL}/login`, data, config); // Post Request to backend
        };

        // Logout Method
        this.logOut = () => {
            $rootScope.userName = null;
            let auth2 = gapi.auth2.getAuthInstance();
            // Signout the user
            auth2.signOut().then(() => {
                // Reset the cookies
                $cookies.put('name', '');
                $cookies.put('uid', '');

                this.user = {};
                $state.go('login'); // Redirect user to Landing page
                $rootScope.toggleButton = true;
                Materialize.toast('Bye! :)', 4000, 'rounded');
            });
        }
    }])

    .directive("googleSignin", ['userService', '$state', '$cookies', '$rootScope', function (userService, $state, $cookies, $rootScope) {
        // Google Signin Directive
        return {
            link: function (scope, element, attrs) {
                let onSuccess = (googleUser) => {
                    $rootScope.preLoad = true;
                    // After login calling login to validate with backend
                    userService.login(googleUser)
                        .then((response) => {
                            let data = response.data;

                            // Set the current user object
                            userService.user.uid = data.uid;
                            userService.user.name = data.name;

                            // Initialize cookie values
                            $cookies.put('uid', userService.user.uid);
                            $cookies.put('name', userService.user.name);

                            Materialize.toast(`Welcome ${data.name}!`, 2000, 'rounded');
                            $state.go('myAccount');
                            $rootScope.toggleButton = false;
                            $rootScope.preLoad = false;

                        }, (error) => {
                            console.log(error);
                        });
                };

                // Render G+ Signin button
                gapi.signin2.render('my-signin2', {
                    'scope': 'profile email',
                    'width': 240,
                    'height': 50,
                    'longtitle': true,
                    'theme': 'dark',
                    'onsuccess': onSuccess
                });
            }
        }
    }]);