'use strict';
// My Account Module
angular.module('pushData.myAccount', ['ui.router', 'ngCookies'])
    .config(['$stateProvider', ($stateProvider) => {
        // My Account State
        $stateProvider.state('myAccount', {
            url: '/myAccount',
            templateUrl: 'myAccount/myAccount.html',
            controller: 'MyAccountCtrl'
        });
    }])

    .controller('MyAccountCtrl', ['$scope', '$http', '$rootScope', '$cookies', 'pushService', 'userService', function ($scope, $http, $rootScope, $cookies, pushService, userService) {
        // My Account Controller
        if ($cookies.get('name') !== '') {
            // Check if user has logged in already
            userService.user.name = $cookies.get('name');
            userService.user.uid = $cookies.get('uid');

        } else {
            $state.go('login');
        }

        $rootScope.userName = userService.user.name; // Get User Name

        // To convert timestamp to local time
        $scope.getDateTime = (ts) => new Date(parseInt(ts)).toLocaleString();

        // DataSet Fetch from DB
        $scope.dataSet = pushService.fetch();

        $scope.add = () => {
            // Push data to DB
            pushService.pushData($scope.content, Date.now())
                .then((response) => {
                    // Add New data 
                    pushService.dataSet.splice(0, 0, response.data.result);
                    $scope.content = "";
                    Materialize.toast(`Posted successfully!`, 4000, 'rounded');
                }, (error) => {
                    console.log(error);
                });
        }

    }])
    .service('pushService', ['$http', 'userService', function ($http, userService) {
        // Service for manipulating DB
        let config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        };
        let baseURL = 'https://lokibg2.pythonanywhere.com';
        var ref = this;
        ref.dataSet = []; // Initialize Data Set

        ref.fetch = () => {
            // Fetch existing data of the current user from DB
            $http.get(`${baseURL}/fetch/${userService.user.uid}`)
                .success(function (data) {
                    for (var i = 0; i < data.result.length; i++) {
                        ref.dataSet.push(data.result[i]);
                    }
                })
                .error(function (data) {
                    console.log(data);
                });
            return ref.dataSet;
        };

        ref.pushData = (text, ts) => {
            // Add new data to DB
            let data = $.param({
                content: text,
                uid: userService.user.uid,
                timestamp: ts
            });
            return $http.post(`${baseURL}/post`, data, config);
        }

    }]);